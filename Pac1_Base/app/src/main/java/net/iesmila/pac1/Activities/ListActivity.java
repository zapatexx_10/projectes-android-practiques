package net.iesmila.pac1.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import net.iesmila.pac1.R;
import net.iesmila.pac1.bd.BaseDadesHelper;
import net.iesmila.pac1.model.Personatge;
import net.iesmila.pac1.model.Rasa;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends Activity implements View.OnClickListener {

    private ListView listViewCartes;
    private static PersonatgeAdapter personatgeAdapter;
    private static List<Personatge> personatges;
    private Button btnNouPersonatge;
    private Button btnFiltre;
    private Button btnNoFiltre;
    private Button btnModifica;
    private Button btnElimina;

    private ImageView imvFrieza;
    private ImageView imvSayajin;
    private ImageView imvNamekian;
    private ImageView imvHuman;
    private ImageView imvMajin;
    private ImageView imvAndroid;

    private EditText edtNomOfici;
    private Personatge p;
    private Integer RasaFiltre = null;
    private String nomOfici;
    private List<Personatge> llistaAux;
    private String nomRasa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listViewCartes = (ListView) findViewById(R.id.lvCartes);
        btnFiltre = (Button) findViewById(R.id.btnFiltra);
        btnNoFiltre = (Button) findViewById(R.id.btnNetejaFiltre);
        btnModifica = (Button) findViewById(R.id.btnModifica);
        btnNouPersonatge = (Button) findViewById(R.id.btnNou);
        btnElimina = (Button) findViewById(R.id.btnDelete);
        edtNomOfici = (EditText) findViewById(R.id.edtNameOfici);
        imvFrieza = (ImageView) findViewById(R.id.imvFrizer);
        imvSayajin = (ImageView) findViewById(R.id.imvSayans);
        imvNamekian = (ImageView) findViewById(R.id.imvNamekian);
        imvHuman = (ImageView) findViewById(R.id.imvHuman);
        imvMajin = (ImageView) findViewById(R.id.imvMajin);
        imvAndroid = (ImageView) findViewById(R.id.imvAndroid);
        llistaAux = new ArrayList<Personatge>();
        personatges = Personatge.getPersonatges(ListActivity.this);
        personatgeAdapter = new PersonatgeAdapter(this, personatges);
        listViewCartes.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listViewCartes.setItemsCanFocus(true);
        listViewCartes.setAdapter(personatgeAdapter);

        btnModifica.setOnClickListener(this);
        btnFiltre.setOnClickListener(this);
        btnNoFiltre.setOnClickListener(this);
        btnNouPersonatge.setOnClickListener(this);
        btnElimina.setOnClickListener(this);


        btnModifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Personatge pSeleccionat = personatges.get(PersonatgeAdapter.getPos());
                if (pSeleccionat.getmImage() == -1) {
                    Toast.makeText(ListActivity.this, "Els Personatges Comprats no poden ser editats!", Toast.LENGTH_SHORT).show();
                } else {
                //Personatge pSeleccionat = personatges.get(PersonatgeAdapter.getPos());
                Intent intentModPersonatges = new Intent(ListActivity.this, MainActivity.class);
                intentModPersonatges.putExtra("tipusintent", 2);
                //intentModPersonatges.putExtra("IDpersonatge",pSeleccionat.getId());
                intentModPersonatges.putExtra("personatge", pSeleccionat);
                startActivity(intentModPersonatges);
                }
            }
        });

        btnFiltre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    nomOfici = edtNomOfici.getText().toString();
                    if (RasaFiltre != null) {
                        llistaAux = omplirLListaPerRasa(RasaFiltre);
                    }
                    if (nomOfici != null) {
                        llistaAux = omplirLlistaPerNomOfici(nomOfici, RasaFiltre);
                    }
                    personatgeAdapter = new PersonatgeAdapter(ListActivity.this, llistaAux);
                    personatgeAdapter.notifyDataSetChanged();
                    listViewCartes.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    listViewCartes.setItemsCanFocus(true);
                    listViewCartes.setAdapter(personatgeAdapter);

                } catch (Exception e) {
                    Log.e("XXXX", e.getMessage());
                }
            }
        });

        btnNoFiltre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RasaFiltre = null;
                nomOfici = null;
                edtNomOfici.setText("");
                personatgeAdapter = new PersonatgeAdapter(ListActivity.this, personatges);
                personatgeAdapter.notifyDataSetChanged();
                listViewCartes.setAdapter(personatgeAdapter);
            }
        });


        //region EscollirRasa


        imvSayajin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RasaFiltre = 1;
                Toast.makeText(ListActivity.this,"Rasa Sayajin Seleccionada",Toast.LENGTH_SHORT).show();
            }
        });
        imvFrieza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RasaFiltre = 2;
                Toast.makeText(ListActivity.this,"Rasa Frieza Seleccionada",Toast.LENGTH_SHORT).show();
            }
        });
        imvAndroid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RasaFiltre = 3;
                Toast.makeText(ListActivity.this,"Rasa Android Seleccionada",Toast.LENGTH_SHORT).show();
            }
        });
        imvHuman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RasaFiltre = 4;
                Toast.makeText(ListActivity.this,"Rasa Humana_Sayajin Seleccionada",Toast.LENGTH_SHORT).show();
            }
        });
        imvNamekian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RasaFiltre = 5;
                Toast.makeText(ListActivity.this,"Rasa Namekiana Seleccionada",Toast.LENGTH_SHORT).show();
            }
        });
        imvMajin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RasaFiltre = 6;
                Toast.makeText(ListActivity.this,"Rasa Majin Seleccionada",Toast.LENGTH_SHORT).show();
            }
        });

        //endregion
    }

    @Override
    protected void onResume() {
        super.onResume();
        personatgeAdapter = new PersonatgeAdapter(this, Personatge.getPersonatgesActualitzats(this));
        listViewCartes.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listViewCartes.setItemsCanFocus(true);
        listViewCartes.setAdapter(personatgeAdapter);
        Log.d("Torno al -->onResume: ", "---------------------");
        personatgeAdapter.notifyDataSetChanged();
    }

    private List<Personatge> omplirLlistaPerNomOfici(String nomOfici, Integer rasaFiltre) {
        List<Personatge> llAuxNomOfici = new ArrayList<Personatge>();
        for (Personatge p : personatges) {
            String nomOficiPers = p.getNomOfici(ListActivity.this, p.getId());
            if (rasaFiltre != null) {
                if (p.getRasa() == rasaFiltre) {
                    if (p.getmNom().contains(nomOfici)) {
                        llAuxNomOfici.add(p);
                    } else if (nomOficiPers.contains(nomOfici)) {
                        llAuxNomOfici.add(p);
                    }
                }
            } else {
                if (p.getmNom().contains(nomOfici)) {
                    llAuxNomOfici.add(p);
                } else if (nomOficiPers.contains(nomOfici)) {
                    llAuxNomOfici.add(p);
                }
            }
        }
        Log.d("XXXX", llAuxNomOfici.toString());
        return llAuxNomOfici;
    }

    private List<Personatge> omplirLListaPerRasa(Integer rasaFiltre) {
        List<Personatge> llAuxRasa = new ArrayList<Personatge>();
        for (Personatge p : personatges) {
            if (p.getRasa() == rasaFiltre) {
                llAuxRasa.add(p);
            }
        }
        return llAuxRasa;
    }

    @Override
    public void onClick(View view) {
        if (view.getTag().equals("btnDelete")) {
            if (PersonatgeAdapter.getLayoutPersDetall() != null && PersonatgeAdapter.getAdapterListSize() > 0) {
                personatges = Personatge.getPersonatgesActualitzats(ListActivity.this);
                Log.e("INDEX ", "_-----------------_" + PersonatgeAdapter.getPos());
                Toast.makeText(ListActivity.this, personatges.get(PersonatgeAdapter.getPos()).getmNom() + " Esborrat..", Toast.LENGTH_SHORT).show();

                BaseDadesHelper.esborrarPersonatge(BaseDadesHelper.getIdPers(personatges.get(PersonatgeAdapter.getPos()).getmNom(), ListActivity.this), ListActivity.this);
                personatgeAdapter.esborrarPersonatgeLlista(PersonatgeAdapter.getPos());

            } else {
                if (PersonatgeAdapter.getAdapterListSize() < 0) {
                    Toast.makeText(ListActivity.this, "No queden Personatges a eliminar...", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(ListActivity.this, "Selecciona un Personatge a eliminar...", Toast.LENGTH_SHORT).show();
            }
        }else if (view.getTag().equals("btnNou")) {
            try {
                Intent intentPersonatges = new Intent(ListActivity.this, MainActivity.class);
                intentPersonatges.putExtra("tipusintent", 1);
                startActivity(intentPersonatges);
            } catch (Exception e) {
                Log.e("Error intent CreaPers", e.getMessage());
            }
        }
    }
    public static PersonatgeAdapter getCartaAdapter() {
        return personatgeAdapter;
    }
}