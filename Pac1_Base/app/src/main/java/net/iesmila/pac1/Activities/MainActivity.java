package net.iesmila.pac1.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;

import net.iesmila.pac1.R;
import net.iesmila.pac1.bd.BaseDadesHelper;
import net.iesmila.pac1.model.Imatge;
import net.iesmila.pac1.model.Ofici;
import net.iesmila.pac1.model.Personatge;
import net.iesmila.pac1.model.Rasa;
import net.iesmila.pac1.model.Sexe;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    private Spinner spnRasa;
    private Spinner spnFase;
    private EditText mNom;
    private EditText mHistoria;
    private RadioButton mRadioEvil,mRadioLawful,mRadioNeutral,mRadioFemale,mRadioMale,mRadioOther;
    private RadioGroup mRadioGroupSexe;
    private RadioGroup mRadioGroupAligment;
    private Button btnMoureEsquerraFoto;
    private Button btnMoureDretaFoto;
    private Button btnMoureEsquerraPersonatge;
    private Button btnMoureDretaPersonatge;
    private Button btnNouPersonatge;
    private Button btnMostraPersonatge;
    private List<Sexe> llistaSexe;
    private List<Ofici> llistaOficis;
    private List<Imatge> llistaImatgesGeneral;
    private List<Imatge> llistaImatgesAux;
    private List<Imatge> llistaImatgesAux2 = new ArrayList<Imatge>();
    private List<Personatge> llistaPersonatges;
    private List<Rasa> llistaRases;
    private ImageView mImatgePersoantge;
    private Intent intentListActvity = null;
    private int tipusIntent;
    int index=0;
    PersonatgeAdapter personatgeAdapter;

    int indexPersonatge=0;
    private SeekBar mSbChar,mSbStr,mSbDext,mSbConst,mSbInt,mSbWisdom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        assignar_elements();
        omplir_adapters_i_llistes();
        llistaPersonatges = PersonatgeAdapter.getLlistaPersonatges();
        indexPersonatge = PersonatgeAdapter.getPos();
        if (indexPersonatge ==-1){
            indexPersonatge=0;
        }
        personatgeAdapter= ListActivity.getCartaAdapter();
        carregarPersonatge(llistaPersonatges.get(indexPersonatge));
        carregaProgressbars(llistaPersonatges.get(indexPersonatge));
        //Log.e("XXX", "PLACAPLACAINSERT", ex)
        intentListActvity = getIntent();
        tipusIntent = getIntent().getIntExtra("tipusintent", -1);
        switch (tipusIntent) {
            case 1:
                try {
                    //Fer insert a la bd
                    netejaCamps();
                    CrearPersonatge();
                    index=0;
                    indexPersonatge = llistaPersonatges.size() - 1;
                    Log.e("XXX", "llistaPersonatges.size()"+llistaPersonatges.size() );
                    carregarPersonatge(llistaPersonatges.get(indexPersonatge));
                }catch(Exception ex){
                    Log.e("XXX", "PLACAPLACAINSERT", ex);
                }
                break;
            /*case 2:
                try{
                    //Passar un int en el intent en comptes del personatge sencer ...........
                    Personatge personatge = (Personatge) intentListActvity.getSerializableExtra("personatge");
                   for(int i =0; i<llistaPersonatges.size();i++){
                        if (llistaPersonatges.get(i).getId() ==personatge.getId()){
                            carregarPersonatge(llistaPersonatges.get(i));
                        }
                   }
                    break;
                } catch (Exception e) {
                    Log.e("XXXX", e.getMessage());
                }*/
        }

        mRadioMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmSexe(1, MainActivity.this, idPersonatge);
                FiltreRasa_Sexe(llistaPersonatges.get(indexPersonatge).getRasa(), 1);
            }
        });

        mRadioFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmSexe(2, MainActivity.this, idPersonatge);
                FiltreRasa_Sexe(llistaPersonatges.get(indexPersonatge).getRasa(), 2);
            }
        });

        mRadioOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmSexe(3,MainActivity.this,idPersonatge);
                FiltreRasa_Sexe(llistaPersonatges.get(indexPersonatge).getRasa(), 3);
            }
        });

        /*Definim els valors maxims i minims dels seekbars*/




        mNom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmNom(mNom.getText().toString(), MainActivity.this, idPersonatge);
            }
        });

        mHistoria.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmHistoria(mHistoria.getText().toString(),MainActivity.this,idPersonatge);
            }
        });

        mRadioEvil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmAlignment(2, MainActivity.this, idPersonatge);
            }
        });

        mRadioNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmAlignment(3, MainActivity.this,idPersonatge);
            }
        });

        mRadioLawful.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmAlignment(1, MainActivity.this,idPersonatge);
            }
        });


        //region setOnSeekBarChangeListener dels Seekbars
        mSbChar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmCharisma(seekBar.getProgress(), MainActivity.this,idPersonatge);
            }
        });
        mSbConst.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmConstitution(seekBar.getProgress(),MainActivity.this,idPersonatge);
            }
        });
        mSbDext.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //UPDATE DEXTERITY
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmDexterity(seekBar.getProgress(), MainActivity.this,idPersonatge);

            }
        });
        mSbInt.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //UPDATE INTELLIGENCE
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmIntelligence(seekBar.getProgress(),MainActivity.this, idPersonatge);

            }
        });
        mSbStr.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmStrength(seekBar.getProgress(), MainActivity.this, idPersonatge);

            }
        });
        mSbWisdom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmWisdom(seekBar.getProgress(),MainActivity.this,idPersonatge);
            }
        });
        //endregion


        //region botons
        btnMoureEsquerraFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if(llistaImatgesAux.size() == 0 || llistaImatgesAux == null){
                    mImatgePersoantge.setImageResource(R.drawable.x);
                    return;
                }
                if (index > 0) {
                    index--;
                } else { index = llistaImatgesAux.size()-1; }
                if(llistaImatgesAux.size() == 0 || llistaImatgesAux == null){
                   mImatgePersoantge.setImageResource(R.drawable.x);
                }
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                int imgRes = llistaImatgesAux.get(index).getmImageResourceId();
                mImatgePersoantge.setImageResource(imgRes);
                llistaPersonatges.get(indexPersonatge).setmImage(imgRes, MainActivity.this, idPersonatge);
            }
        });

       btnMoureDretaFoto.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               try {

                   Log.e("XXX>>", "index:"+ index + "llistaImatgesAux.size2:"+llistaImatgesAux.size());
                   if (index < llistaImatgesAux.size()-1) {
                       index++;
                   } else { index = 0; }
                   Log.e("XXX>><<", "index:" + index);
                   int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                   int imgRes = llistaImatgesAux.get(index).getmImageResourceId();
                   mImatgePersoantge.setImageResource(imgRes);
                   llistaPersonatges.get(indexPersonatge).setmImage(imgRes, MainActivity.this, idPersonatge);
               }  catch (Exception ex) {
                   Log.e("XXX", "PUFFFF", ex);
               }



           }

       });

        btnMoureDretaPersonatge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexPersonatge++;
                try {
                    Personatge p = llistaPersonatges.get(indexPersonatge);
                    carregarPersonatge(p);

                } catch (IndexOutOfBoundsException a) {
                    indexPersonatge = -1;

                }
            }
        });

        btnMoureEsquerraPersonatge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexPersonatge--;
                try {
                    Personatge p = llistaPersonatges.get(indexPersonatge);
                    carregarPersonatge(p);
                } catch (IndexOutOfBoundsException a) {
                    indexPersonatge = llistaPersonatges.size();
                }
            }
        });

        btnNouPersonatge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               try {
                   //Fer insert a la bd
                   netejaCamps();
                   CrearPersonatge();
                   index=0;
                   indexPersonatge = llistaPersonatges.size() - 1;
                   carregarPersonatge(llistaPersonatges.get(indexPersonatge));
               }catch(Exception ex){
                   Log.e("XXX", "PLACAPLACAINSERT", ex);
               }
            }
        });

        btnMostraPersonatge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    Personatge p = llistaPersonatges.get(indexPersonatge);

                    Intent intentPersonatges = new Intent(MainActivity.this, MainActivityVista.class);
                    intentPersonatges.putExtra("Personatge",p);
                    startActivity(intentPersonatges);
                }catch(Exception ex){
                    Log.e("XXX", "PUFFFF MOSTRA PERSONATGE", ex);
                }

            }
        });
        //endregion

        spnRasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmRasa(llistaRases.get(pos).getId(),MainActivity.this,idPersonatge);
                FiltreRasa_Sexe(llistaPersonatges.get(indexPersonatge).getRasa(), llistaPersonatges.get(indexPersonatge).getmSexe());
                String rasaSeleccionada = spnRasa.getSelectedItem().toString();

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spnFase.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int idPersonatge = llistaPersonatges.get(indexPersonatge).getId();
                llistaPersonatges.get(indexPersonatge).setmOfici(llistaOficis.get(i).getCodi(),MainActivity.this,idPersonatge);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void CrearPersonatge() {
    try{
        BaseDadesHelper dbh =
                new BaseDadesHelper(MainActivity.this, "BaseDades", null, 1);

        SQLiteDatabase db = dbh.getWritableDatabase();


        db.execSQL("INSERT INTO Personatge (NOM , RASA_ID,SEXE_ID, OFICI_ID, ALIGNMENT_ID,IMATGE,CONSTITUTION,STRENGTH, DEXTERITY, WISEDOM, INTELLIGENCE, CHARISMA,HISTORY, ID_BOTIGA, ARXIU_IMG_DESC) " +
                "VALUES ('Personatge Nou'," + null + "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + "," + null + ")");


        db.close();
        llistaPersonatges.clear();
        llistaPersonatges=Personatge.getPersonatgesActualitzats(MainActivity.this);
    }catch (Exception e){
        Log.e("XXX", "PUFFFF CREA PERSONATGE", e);
    }

    }

    private void carregarPersonatge(Personatge p) {
        try {
            Log.e("XXX", "INICI");
            mImatgePersoantge.setImageResource(p.getmImage());
            Log.e("XXX", "MIG1");
            carregaProgressbars(p);
            Log.e("XXX", "MIG2");
            mNom.setText(p.getmNom());
            mHistoria.setText(p.getmHistoria());
            seleccionarRadioAligmentPersonatge(p);
            seleccionarRadioSexePersonatge(p);
            selectRace(p);
            selectFase(p);
            FiltreRasa_Sexe(p.getRasa(), p.getmSexe());
            Log.e("XXX", "FI");
        }  catch (Exception ex) {
            Log.e("XXX", "PUFFFF", ex);
        }
    }

    private void carregaProgressbars(Personatge p) {
        mSbWisdom.setProgress(p.getmWisdom());
        mSbStr.setProgress(p.getmStrength());
        mSbInt.setProgress(p.getmIntelligence());
        mSbDext.setProgress(p.getmDexterity());
        mSbChar.setProgress(p.getmCharisma());
        mSbConst.setProgress(p.getmConstitution());
    }

    private void omplir_adapters_i_llistes() {
          /*Omplim els adapters amb les raçes i les fases que hi ha */

        llistaImatgesGeneral =getImatges();
        llistaImatgesAux = new ArrayList<>();
        llistaPersonatges = Personatge.getPersonatges(MainActivity.this);
        llistaOficis =getOficis();
        llistaRases = getRases();
        Log.d("XXXX",llistaRases.toString());
        spnRasa.setAdapter(new ArrayAdapter<Rasa>(this, android.R.layout.simple_spinner_item, llistaRases));
        spnFase.setAdapter(new ArrayAdapter<Ofici>(this, android.R.layout.simple_spinner_item,llistaOficis));
        definirmaxMin(mSbChar);
        definirmaxMin(mSbConst);
        definirmaxMin(mSbDext);
        definirmaxMin(mSbInt);
        definirmaxMin(mSbStr);
        definirmaxMin(mSbWisdom);
        llistaImatgesAux= llistaImatgesGeneral;
    }

    private void seleccionarRadioAligmentPersonatge(Personatge p) {
        if(p.getmAlignment()==2){
            mRadioEvil.setChecked(true);
        }else if (p.getmAlignment()==3){
            mRadioNeutral.setChecked(true);
        }else if (p.getmAlignment()==1){
            mRadioLawful.setChecked(true);
        }
    }

    private void seleccionarRadioSexePersonatge(Personatge p) {
        if(p.getmSexe()==2){
            mRadioFemale.setChecked(true);
            FiltreRasa_Sexe(p.getRasa(), 2);
        }else if (p.getmSexe()==1){
            mRadioMale.setChecked(true);
            FiltreRasa_Sexe(p.getRasa(), 1);
        }else if (p.getmSexe()==3){
            mRadioOther.setChecked(true);
            FiltreRasa_Sexe(p.getRasa(), 3);
        }
    }


    private void assignar_elements() {

        spnRasa= (Spinner)findViewById(R.id.spnRasa);
        spnFase=(Spinner)findViewById(R.id.spnFase);
        mImatgePersoantge =(ImageView)findViewById(R.id.imvPersonatge);
        btnMoureEsquerraFoto = (Button) findViewById(R.id.btnPassarImatgeEsquerra);
        btnMoureDretaFoto =(Button)findViewById(R.id.btnPassarImatgeDreta);
        btnMoureEsquerraPersonatge = (Button) findViewById(R.id.btnPassarEsquerra);
        btnMoureDretaPersonatge =(Button)findViewById(R.id.btnPassarDreta);
        btnNouPersonatge = (Button) findViewById(R.id.btnNouPersonatge);
        btnMostraPersonatge=(Button)findViewById(R.id.btnVeurePersonatge);
        mSbChar=(SeekBar)findViewById(R.id.skbCharisma);
        mSbConst=(SeekBar)findViewById(R.id.skbConstitution);
        mSbDext=(SeekBar)findViewById(R.id.skbDexterity);
        mSbInt=(SeekBar)findViewById(R.id.skbntelligence);
        mSbStr=(SeekBar)findViewById(R.id.skbStrenght);
        mSbWisdom=(SeekBar)findViewById(R.id.skbWisedom);
        mNom=(EditText)findViewById(R.id.edtNomPersoatge);
        mHistoria=(EditText)findViewById(R.id.edtHistoria);
        /*Radiobuttons comportament*/
        mRadioEvil=(RadioButton)findViewById(R.id.rbtnEvil);
        mRadioLawful=(RadioButton)findViewById(R.id.rbtnLawful);
        mRadioNeutral=(RadioButton)findViewById(R.id.rbtnNeutral);
         /*Radiobuttons sexe*/
        mRadioMale=(RadioButton)findViewById(R.id.rbtnMale);
        mRadioFemale=(RadioButton)findViewById(R.id.rbtnFemale);
        mRadioOther=(RadioButton)findViewById(R.id.rbtnOther);
        mRadioGroupSexe=(RadioGroup)findViewById(R.id.GroupSexe);
        mRadioGroupAligment=(RadioGroup)findViewById(R.id.GroupAligment);
    }

    private void selectRace(Personatge p) {
        Log.e("XXXX", ">Select Race:" + p.toString());
        Log.e("XXXX", ">Select Race:" + p.getRasa());


        int pos =-1;
        for(int i=0;i<llistaRases.size();i++) {
            if(llistaRases.get(i).getId()== p.getRasa()) {
                pos = i;
            }
        }
        Log.e("XXXX", ">Select Race pos:" + pos);

        spnRasa.setSelection(pos);
        FiltreRasa_Sexe(p.getRasa(), p.getmSexe());
        //Treure això si no funciona
        index=0;

    }

    private void selectFase(Personatge p) {
        int pos =-1;
        for(int i=0;i<llistaOficis.size();i++) {
            if(llistaOficis.get(i).getCodi()== p.getmOfici()) {
                pos = i;
            }
        }
        spnFase.setSelection(pos);

    }


    private void FiltreRasa_Sexe(int rasa, int sexe){
        llistaImatgesAux.clear();
        ArrayList<Imatge> imatgesSQL= Imatge.getmImatges();

        int id;
        int sexe_id;
        int rasa_id;
        int foto;

        BaseDadesHelper usdbh =
                new BaseDadesHelper(this, "BaseDades", null, 1);

        SQLiteDatabase db = usdbh.getReadableDatabase();

        Cursor c1 = db.rawQuery("select * from IMATGE where sexe_id="+sexe+" and rasa_id="+rasa+"",null);
        while(c1.moveToNext()){
            id=c1.getInt(c1.getColumnIndex("ID"));
            sexe_id=c1.getInt(c1.getColumnIndex("SEXE_ID"));
            rasa_id=c1.getInt(c1.getColumnIndex("RASA_ID"));
            foto=c1.getInt(c1.getColumnIndex("SRC"));
            imatgesSQL.add(new Imatge(id,sexe_id,rasa_id,foto));
            Log.d("BERNAT",c1.getString(c1.getColumnIndex("ID")));
        }
        db.close();
        llistaImatgesAux=imatgesSQL;
    }

    private void definirmaxMin(SeekBar mskb) {
        mskb.setProgress(0);
        mskb.incrementProgressBy(10);
        mskb.setMax(100);
    }

    private void netejaCamps(){
        mSbChar.setProgress(0);
        mSbDext.setProgress(0);
        mSbInt.setProgress(0);
        mSbConst.setProgress(0);
        mSbWisdom.setProgress(0);
        mSbStr.setProgress(0);
        mNom.setText("");
        mHistoria.setText("");
        mRadioGroupAligment.clearCheck();
        mRadioGroupSexe.clearCheck();
        spnFase.setPrompt("Select Fase");
        spnRasa.setPrompt("Select Raça");
        spnRasa.setSelection(0);
        spnFase.setSelection(0);
    }
    public ArrayList<Imatge> getImatges(){

        ArrayList<Imatge> imatgesSQL= Imatge.getmImatges();

        int id;
        int sexe_id;
        int rasa_id;
        int foto;

        BaseDadesHelper usdbh =
                new BaseDadesHelper(this, "BaseDades", null, 1);

        SQLiteDatabase db = usdbh.getReadableDatabase();

        Cursor c1 = db.rawQuery("select * from IMATGE",null);
        try{
            while(c1.moveToNext()){
                id=c1.getInt(c1.getColumnIndex("ID"));
                sexe_id=c1.getInt(c1.getColumnIndex("SEXE_ID"));
                rasa_id=c1.getInt(c1.getColumnIndex("RASA_ID"));
                foto=c1.getInt(c1.getColumnIndex("SRC"));
                imatgesSQL.add(new Imatge(id,sexe_id,rasa_id,foto)); //Modificar Imatge
                Log.d("BERNAT",c1.getString(c1.getColumnIndex("ID")));
            }
        }catch(Exception e){
            Log.e("BERNAT", e.getMessage());
        }
        db.close();
        return imatgesSQL;
    }

    public ArrayList<Ofici> getOficis(){

        ArrayList<Ofici> oficisSQL= Ofici.getOficis();
        int mCodi;
        String mOfici;
        BaseDadesHelper usdbh = new BaseDadesHelper(MainActivity.this, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getReadableDatabase();
        Cursor c1 = db.rawQuery("select * from OFICI",null);
        try{
            while(c1.moveToNext()){
                mCodi=c1.getInt(c1.getColumnIndex("ID"));
                mOfici=c1.getString(c1.getColumnIndex("NAME"));
                oficisSQL.add(new Ofici(mCodi,mOfici));
                Log.d("MARC",c1.getString(c1.getColumnIndex("ID")));
            }

        }catch(Exception e){
            Log.e("BERNAT", e.getMessage());
        }
        db.close();
        return oficisSQL;
    }

    public ArrayList<Rasa> getRases(){

        ArrayList<Rasa> rasesSQL= Rasa.getRases();
        int mCodi;
        String mOfici;
        int mFoto;
        BaseDadesHelper usdbh = new BaseDadesHelper(MainActivity.this, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getReadableDatabase();
        Cursor c1 = db.rawQuery("select * from RASA",null);
        try{
            while(c1.moveToNext()){
                mCodi=c1.getInt(c1.getColumnIndex("ID"));
                mOfici=c1.getString(c1.getColumnIndex("NAME"));
                mFoto=c1.getInt(c1.getColumnIndex("IMATGE"));//Posar el camp a a BD
                rasesSQL.add(new Rasa(mCodi,mOfici,mFoto));
                Log.d("MARC",c1.getString(c1.getColumnIndex("ID")));
            }

        }catch(Exception e){
            Log.e("BERNAT", e.getMessage());
        }
        db.close();
        return rasesSQL;
    }
}
