package net.iesmila.pac1.Activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import net.iesmila.pac1.R;
import net.iesmila.pac1.model.Personatge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcc on 24/05/2016.
 */
public class ShopActivity extends ActionBarActivity {
    private ListView listViewStore;
    private ImageButton btnBack;
    private ImageButton btnCompra;
    private TendaAdapter mStoreAdapter;
    private List<Personatge> mLlistaPersonatges = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenda);

        listViewStore= (ListView)findViewById(R.id.lvStorePersonatges);
        btnBack =(ImageButton)findViewById(R.id.btnStoreEnrere);
        btnCompra=(ImageButton)findViewById(R.id.btnStoreCompra);

        mLlistaPersonatges= Personatge.getPersonatgesStore(ShopActivity.this);
        Log.d("Num de personatges", mLlistaPersonatges.size() + "---------");
        mStoreAdapter= new TendaAdapter(this,mLlistaPersonatges);
        listViewStore.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listViewStore.setItemsCanFocus(true);
        listViewStore.setAdapter(mStoreAdapter);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnCompra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLlistaPersonatges.size()<1){
                    Toast.makeText(ShopActivity.this, "Ja no hi ha mes personatges...", Toast.LENGTH_SHORT).show();
                }else {
                    Log.d("Comprat", mLlistaPersonatges.get(TendaAdapter.getmPos()).getmNom());
                    mStoreAdapter.esborrarPersonatgeStore(TendaAdapter.getmPos());
                    mLlistaPersonatges = Personatge.getPersonatgesStore(ShopActivity.this);//Actualitzem la llista i posem els persoantges No comprats encara
                }
            }
        });

    }
}
