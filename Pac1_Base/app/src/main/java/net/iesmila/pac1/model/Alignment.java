package net.iesmila.pac1.model;

import java.io.Serializable;

/**
 * Created by BERNAT on 05/02/2016.
 */

public class Alignment implements Serializable{
    int id;
    String nom;

    public Alignment(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Alignment{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Alignment alignment = (Alignment) o;

        return id == alignment.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
/*public enum Alignment {
    LAWFUL, NEUTRAL, EVIL
}*/
