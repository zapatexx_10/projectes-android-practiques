package net.iesmila.pac1.network;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import net.iesmila.pac1.bd.BaseDadesHelper;
import net.iesmila.pac1.model.Personatge;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcc on 23/05/2016.
 */
public class PersonatgeDataLoader {
    private Context context;

    public PersonatgeDataLoader(Context context){
        this.context = context;
    }

    public List<Personatge> loadData(String url) throws Exception {
        Log.e("PERSONATGE JSON: ", "Entrem al loadData");
        ServiceHandler sh = new ServiceHandler();
        String json = sh.makeServiceCall(url, ServiceHandler.GET);

        Log.e("PERSONATGE JSON: ","llegim URL i començema fer el JSONObj----------");
        JSONObject c = new JSONObject(json);
        JSONArray list = c.getJSONArray("items");
        List<Personatge> personatgesTenda = new ArrayList<>();

        try {
            for (int i = 0; i < list.length(); i++) {
                JSONObject p = list.getJSONObject(i);
               /* Personatge(String mNom, int mRasa, int mSexe, int mOfici, int mAlignment, int mImage, int mConstitution, int mStrength,
                int mDexterity, int mWisdom, int mIntelligence, int mCharisma,
                String mHistoria, Integer id_botiga, Integer arxiu_img_desc)*/
                Personatge personatge = new Personatge(
                        p.getString("nom"),
                        p.getInt("rasa"),
                        p.getInt("sexe"),
                        p.getInt("ofici_id"),
                        p.getInt("aligment"),
                        -1,
                        p.getInt("constitution"),
                        p.getInt("strenght"),
                        p.getInt("dexterity"),
                        p.getInt("wisdom"),
                        p.getInt("intelligence"),
                        p.getInt("charisma"),
                        p.getString("descripcio"),
                        p.getInt("id_store"),
                        p.getString("imatge")
                        );

                if(!BaseDadesHelper.existeixPersonatge(personatge.getmNom(), context)){
                    personatgesTenda.add(personatge);
                    Log.d("PERSONATGE JSON: ", personatge.toString());
                }
            }
        }catch (Exception e){
            Log.e("PLACAPLACA JSON",e.getMessage());}
            Log.e("PERSONATGE JSON: ", "FI ----------");
            Personatge.setmPersonatgesStore(personatgesTenda);
            return personatgesTenda;
    }

}
