package net.iesmila.pac1.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import net.iesmila.pac1.AssyncTask.TendaAsyncTask;
import net.iesmila.pac1.R;
import net.iesmila.pac1.model.Personatge;

/**
 * Created by Marcc on 05/04/2016.
 */

public class ShopActivityLoading extends ActionBarActivity {

    private ProgressBar mPersDownloading;
    private TendaAsyncTask mTendaAsTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_store_loading);


        mPersDownloading = (ProgressBar) findViewById(R.id.pgrDownloading);
        mPersDownloading.setVisibility(View.INVISIBLE);
        mTendaAsTask = new TendaAsyncTask(this);
        mTendaAsTask.execute("Personatge");

    }

    public void mostrarProgressBar() {
        mPersDownloading.setVisibility(View.VISIBLE);
    }

    public void mostrarErrorDescarrega() {
        mPersDownloading.setVisibility(View.INVISIBLE);
        Toast.makeText(this, "PlACAPLACA Error mostraDesc", Toast.LENGTH_LONG).show();
    }

    public void fiDescarrega(Personatge person) {
        mPersDownloading.setVisibility(View.INVISIBLE);
        Toast.makeText(this, "Descarrega Completada", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ShopActivityLoading.this, ShopActivity.class);
        (ShopActivityLoading.this).finish();
        startActivity(intent);
    }
}
