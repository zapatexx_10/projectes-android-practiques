package net.iesmila.pac1.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*public enum Rasa {
    SAYAJIN, HUMAN_SAYAJIN, ANDROID, MAJIN, NAMEKIAN,FRIEZA_RACE
}*/
public class Rasa implements Serializable{
    int id;
    String nom;
    int foto;
    private static ArrayList<Rasa> mRases;

    public static ArrayList<Rasa> getRases(){
        if(mRases==null) {
            mRases= new ArrayList<Rasa>();
        }
        return mRases;
    }

    public static List<Rasa> getmRases(){
        if(mRases==null) {
            mRases= new ArrayList<Rasa>();
        }
        return mRases;
    }

    public Rasa(int id, String nom,int foto) {
        this.id = id;
        this.nom = nom;
        this.foto=foto;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rasa rasa = (Rasa) o;

        return id == rasa.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
