package net.iesmila.pac1.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by BERNAT on 05/02/2016.
 */
public class Imatge implements Serializable {
    private int id;
    private int mSexe;
    private int mRasa;
    private int mImageResourceId;

    private static ArrayList<Imatge> llistaImatges = null;

    public List<Imatge> getImatges() {
        if(llistaImatges==null) {
            llistaImatges= new ArrayList<Imatge>();
        }
        return llistaImatges;
    }

    public static ArrayList<Imatge> getmImatges() {
        if(llistaImatges==null) {
            llistaImatges= new ArrayList<Imatge>();
        }
        return llistaImatges;
    }

    public Imatge(int id, int mSexe, int mRasa, int mImageResourceId) {
        this.id = id;
        this.mSexe = mSexe;
        this.mRasa = mRasa;
        this.mImageResourceId = mImageResourceId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getmSexe() {
        return mSexe;
    }

    public void setmSexe(int mSexe) {
        this.mSexe = mSexe;
    }

    public int getmRasa() {
        return mRasa;
    }

    public void setmRasa(int mRasa) {
        this.mRasa = mRasa;
    }

    public int getmImageResourceId() {
        return mImageResourceId;
    }

    public void setmImageResourceId(int mImageResourceId) {
        this.mImageResourceId = mImageResourceId;
    }
}