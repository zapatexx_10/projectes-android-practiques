package net.iesmila.pac1.model;

import java.io.Serializable;
/*public enum Sexe {
    MALE, FEMALE, OTHER
}*/
public class Sexe implements Serializable{
    int id;
    String sexe_typ;

    public Sexe(int id, String sexe_typ) {
        this.id = id;
        this.sexe_typ = sexe_typ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSexe_typ() {
        return sexe_typ;
    }

    public void setSexe_typ(String sexe_typ) {
        this.sexe_typ = sexe_typ;
    }

    @Override
    public String toString() {
        return "Sexe{" +
                "id=" + id +
                ", sexe_typ='" + sexe_typ + '\'' +
                '}';
    }
}
