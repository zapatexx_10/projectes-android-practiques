package net.iesmila.pac1.Activities;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import net.iesmila.pac1.R;
import net.iesmila.pac1.bd.BaseDadesHelper;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        BaseDadesHelper dbh = new BaseDadesHelper(this, "BaseDades",null, 1);
        SQLiteDatabase baseDades = dbh.getReadableDatabase();

        Log.d("MARC", "llego a activity");
        Thread thread = new Thread() {

            @Override
            public void run() {
                synchronized (this) {
                    try {
                        wait(4000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        startActivity(new Intent(getBaseContext(),
                                ActivityChooseOption.class));
                        finish();
                    }
                }
            }
        };

        // start thread
        thread.start();
    }

    @Override
    public void onBackPressed() {

    }

}
