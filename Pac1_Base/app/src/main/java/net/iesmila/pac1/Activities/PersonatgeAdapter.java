package net.iesmila.pac1.Activities;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.iesmila.pac1.LazyLoading.ImageLoader;
import net.iesmila.pac1.R;
import net.iesmila.pac1.bd.BaseDadesHelper;
import net.iesmila.pac1.model.Personatge;

import java.util.List;

/**
 * Created by Marcc on 27/03/2016.
 */
public class PersonatgeAdapter extends ArrayAdapter<Personatge> {

    private static List<Personatge> mLlistaPersonatges;
    private static int index;
    private static LinearLayout layoutPers;
    private static ViewGroup parentListView = null;
    private static ArrayAdapter<Personatge> aux;
    private static String url= "http://vps97640.ovh.net/~m2.mzapata/";
    private ImageLoader imLoader;

    public PersonatgeAdapter(Context context, List<Personatge> pLlistaPers){
        //Potser s'han de passar els dos botons, el de esborrar i el de modificar...

        super(context, R.layout.item_carta, R.id.imvPersonatgeVista, pLlistaPers);
        mLlistaPersonatges = pLlistaPers;
        aux= this;
    }

    private View.OnClickListener mOnButtonClicked = new View.OnClickListener() {

        public void onClick(View v) {

            layoutPers =(LinearLayout) v;

            Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.animacio_item_fila);
            v.setAnimation(anim);
            anim.start();

            ListView listView = (ListView) parentListView;
            boolean isChecked = listView.isItemChecked(index);
            listView.setItemChecked(index,!isChecked);
        }
    };

    public void setPersonatges(List<Personatge> pPersonatges){
        mLlistaPersonatges = pPersonatges;
        index = 0;
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View vistaReciclada, final ViewGroup parentLV) {
        parentListView = parentLV;
        final LinearLayout llyFila;

        if(vistaReciclada!=null) {
            llyFila = (LinearLayout)vistaReciclada;
        }else {
            LayoutInflater factory = LayoutInflater.from(this.getContext());
            llyFila = (LinearLayout) factory.inflate(R.layout.item_carta, parentLV, false);
        }
        imLoader = new ImageLoader(getContext());
        index= position;
        ImageView mImatgePersoantge =(ImageView)llyFila.findViewById(R.id.imvPersonatgeCarta);
        ImageView mImatgeSexe =(ImageView)llyFila.findViewById(R.id.imvSexe);
        ImageView mImatgeRasa =(ImageView)llyFila.findViewById(R.id.imvRasa);
        Button mAlignment = (Button)llyFila.findViewById(R.id.btnAlignment);
        TextView mNom= (TextView)llyFila.findViewById(R.id.txvPersonatge);
        ProgressBar mPbChar=(ProgressBar)llyFila.findViewById(R.id.prbCharisma);
        ProgressBar mPbStr=(ProgressBar)llyFila.findViewById(R.id.prbStrenght);
        ProgressBar mPbDext=(ProgressBar)llyFila.findViewById(R.id.prbDexterity);
        ProgressBar mPbConst=(ProgressBar)llyFila.findViewById(R.id.prbConstitution);
        ProgressBar mPbInt=(ProgressBar)llyFila.findViewById(R.id.prbntelligence);
        ProgressBar mPbWisdom=(ProgressBar)llyFila.findViewById(R.id.prbWisedom);
        TextView mFase= (TextView)llyFila.findViewById(R.id.txvFase);
        llyFila.setTag(index);
        Personatge personatge = mLlistaPersonatges.get(index);

       if (personatge.getmImage()==-1){
            imLoader.DisplayImage(url+personatge.getArxiu_img_desc(),mImatgePersoantge);
        }else{
            mImatgePersoantge.setImageResource(personatge.getmImage());
       }
        Log.d("marc", "construeixo personatge " + personatge.getmNom());

        mNom.setText(personatge.getmNom());

        //NO FUNCIONAN AQUESTS CUSTOM PROGRESSBAR.... pta bida tt
        /*customProgressBar(mPbStr, personatge.getmStrength());
        customProgressBar(mPbConst, personatge.getmConstitution());
        customProgressBar(mPbChar, personatge.getmCharisma());
        customProgressBar(mPbInt, personatge.getmIntelligence());
        customProgressBar(mPbDext, personatge.getmDexterity());
        customProgressBar(mPbWisdom, personatge.getmWisdom());*/

        mPbStr.setProgress(personatge.getmStrength());
        mPbConst.setProgress(personatge.getmConstitution());
        mPbChar.setProgress(personatge.getmCharisma());
        mPbInt.setProgress(personatge.getmIntelligence());
        mPbDext.setProgress(personatge.getmDexterity());
        mPbWisdom.setProgress(personatge.getmWisdom());

        switch (personatge.getmOfici()){
            case 1:mFase.setText("Base"); break;
            case 2:mFase.setText("SSJ1"); break;
            case 3:mFase.setText("SSJ2"); break;
            case 4:mFase.setText("SSJ3"); break;
            case 5:mFase.setText("SSJGOD"); break;
            case 6:mFase.setText("Perfect form");break;
            case 7:mFase.setText("Imperfect form");
        }

        switch (personatge.getmSexe()){
            case 1: mImatgeSexe.setBackgroundResource(R.drawable.masculino); break;
            case 2: mImatgeSexe.setBackgroundResource(R.drawable.femenino);break;
            case 3: mImatgeSexe.setBackgroundResource(R.drawable.othersex);
        }

        switch (personatge.getmAlignment()){
            case 1:mAlignment.setBackgroundResource(R.drawable.like); break;
            case 2:mAlignment.setBackgroundResource(R.drawable.dislike);break;
            case 3:mAlignment.setBackgroundResource(R.drawable.equals);
        }


        switch (personatge.getRasa()){
            case 1: mImatgeRasa.setBackgroundResource(R.drawable.sayajinlogo); break;
            case 2: mImatgeRasa.setBackgroundResource(R.drawable.friezaforcelogo);break;
            case 3: mImatgeRasa.setBackgroundResource(R.drawable.redribbonlogo);break;
            case 4: mImatgeRasa.setBackgroundResource(R.drawable.humanslogo);break;
            case 6: mImatgeRasa.setBackgroundResource(R.drawable.majinlogo);break;
            case 5: mImatgeRasa.setBackgroundResource(R.drawable.namekianlogo);
        }
        llyFila.setOnClickListener(mOnButtonClicked);
        return llyFila;
    }

    /*public void customProgressBar(ProgressBar pb, int pos){
        final float[] roundedCorners = new float[] { 5, 5, 5, 5, 5, 5, 5, 5 };
        ShapeDrawable pgDrawable = new ShapeDrawable(new RoundRectShape(roundedCorners, null,null));
        String colorBarra = "colors";
        if(pos>=0&&pos<25){
            colorBarra = "#ebd21310";
        }else if(pos>=25&&pos<50){
            colorBarra = "#FF946B23";
        }else if(pos>=50&&pos<75){
            colorBarra = "#4b7901";
        }else if(pos>=75&&pos<=100){
            colorBarra = "#06991c";
        }
        pgDrawable.getPaint().setColor(Color.parseColor(colorBarra));
        ClipDrawable progress = new ClipDrawable(pgDrawable, Gravity.LEFT, ClipDrawable.HORIZONTAL);
        pb.setProgressDrawable(progress);
        pb.setBackgroundDrawable(getContext().getResources().getDrawable(android.R.drawable.progress_horizontal));
        pb.setProgress(pos);
    }*/

    public static ViewGroup getParentListView(){
        return parentListView;
    }
    public static LinearLayout getLayoutPersDetall(){
        return layoutPers;
    }
    public static List<Personatge> getLlistaPersonatges(){
        return mLlistaPersonatges;
    }

    public static int getPos() {
        if (layoutPers ==null){
            return -1;
        }
        return (int)layoutPers.getTag();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public static int getAdapterListSize(){
        return mLlistaPersonatges.size();
    }

    public  void esborrarPersonatgeLlista(int pos){
        BaseDadesHelper.esborrarPersonatge(pos,getContext());
        mLlistaPersonatges.remove(pos);
        this.notifyDataSetChanged();
    }

    public void afegirPersonatgeLlista(Personatge pers){
        mLlistaPersonatges.add(pers);
        this.notifyDataSetChanged();
    }

    public static ArrayAdapter<Personatge> getAdapter(){
        return aux;
    }
}


