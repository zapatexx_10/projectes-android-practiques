package net.iesmila.pac1.bd;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import net.iesmila.pac1.R;
import net.iesmila.pac1.model.Personatge;

public class BaseDadesHelper extends SQLiteOpenHelper {
   // private static SQLiteDatabase baseDades;

    //Sentencia SQL para crear la tabla de canciones
    String CreateSexe = 	 "CREATE TABLE SEXE (ID INTEGER PRIMARY KEY AUTOINCREMENT,SEX_TYPE TEXT)";

    String CreateOfici = 	 "CREATE TABLE OFICI (ID INTEGER PRIMARY KEY AUTOINCREMENT,NAME  TEXT)";

    String CreateRasa =     "CREATE TABLE RASA (ID INTEGER PRIMARY KEY AUTOINCREMENT,NAME  TEXT,IMATGE INTEGER)";

    String CreateAlignment = "CREATE TABLE ALIGNMENT (ID INTEGER PRIMARY KEY AUTOINCREMENT,NAME  TEXT)";

    String CreateImatge = 	 "CREATE TABLE IMATGE (ID INTEGER PRIMARY KEY AUTOINCREMENT,SEXE_ID INTEGER,RASA_ID INTEGER,SRC INTEGER,FOREIGN KEY(RASA_ID) REFERENCES RASA(ID),FOREIGN KEY(SEXE_ID) REFERENCES SEXE(ID))";

    String CreatePersonatge = "CREATE TABLE PERSONATGE (ID INTEGER PRIMARY KEY AUTOINCREMENT, NOM TEXT, RASA_ID INTEGER,SEXE_ID INTEGER, OFICI_ID INTEGER, ALIGNMENT_ID INTEGER,IMATGE INTEGER,CONSTITUTION INTEGER,STRENGTH 	INTEGER, DEXTERITY INTEGER, WISEDOM INTEGER, INTELLIGENCE INTEGER, CHARISMA INTEGER,HISTORY TEXT, ID_BOTIGA INTEGER, ARXIU_IMG_DESC TEXT, FOREIGN KEY(RASA_ID) REFERENCES RASA(ID),FOREIGN KEY(SEXE_ID) REFERENCES SEXE(ID), FOREIGN KEY(OFICI_ID) REFERENCES OFICI(ID), FOREIGN KEY(ALIGNMENT_ID) REFERENCES ALIGNMENT(ID))";


    public BaseDadesHelper(Context contexto, String nombre,
                           CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

            Log.d("BERNAT","PASA PER AqUI CABRON");
            //Se ejecuta la sentencia SQL de creación de la tabla i la insercion de valores
            createTables(db, CreateSexe, CreateOfici, CreateRasa, CreateAlignment, CreateImatge, CreatePersonatge);
            insertValues(db);
    }

    private void insertValues(SQLiteDatabase baseDades) {
        //Alignment
        baseDades.execSQL("INSERT INTO ALIGNMENT VALUES (1,'Lawful')");
        baseDades.execSQL("INSERT INTO ALIGNMENT VALUES (2,'Evil')");
        baseDades.execSQL("INSERT INTO ALIGNMENT VALUES (3,'Neutral')");
        //RASA
        baseDades.execSQL("INSERT INTO RASA VALUES (1,'Sayajin',"+ R.drawable.sayajinlogo+")");
        baseDades.execSQL("INSERT INTO RASA VALUES (2,'Frieza Race',"+ R.drawable.friezaforcelogo+")");
        baseDades.execSQL("INSERT INTO RASA VALUES (3,'Android',"+ R.drawable.redribbonlogo+")");
        baseDades.execSQL("INSERT INTO RASA VALUES (4,'Human Sayajin',"+ R.drawable.humanslogo+")");
        baseDades.execSQL("INSERT INTO RASA VALUES (5,'Namekian',"+ R.drawable.namekianlogo+")");
        baseDades.execSQL("INSERT INTO RASA VALUES (6,'Majin',"+ R.drawable.majinlogo+")");
        //SEXE
        baseDades.execSQL("INSERT INTO SEXE VALUES (1,'Male')");
        baseDades.execSQL("INSERT INTO SEXE VALUES (2,'Female')");
        baseDades.execSQL("INSERT INTO SEXE VALUES (3,'Other')");
        //OFICI
        baseDades.execSQL("INSERT INTO OFICI VALUES (1,'Base')");
        baseDades.execSQL("INSERT INTO OFICI VALUES (2,'SSJ1')");
        baseDades.execSQL("INSERT INTO OFICI VALUES (3,'SSJ2')");
        baseDades.execSQL("INSERT INTO OFICI VALUES (4,'SSJ3')");
        baseDades.execSQL("INSERT INTO OFICI VALUES (5,'SSJGOD')");
        baseDades.execSQL("INSERT INTO OFICI VALUES (6,'Perfect form')");
        baseDades.execSQL("INSERT INTO OFICI VALUES (7,'Imperfect form')");
        //IMATGES
        baseDades.execSQL("INSERT INTO IMATGE VALUES (1,1,1,"+ R.drawable.gokuneutral+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (2,1,1,"+ R.drawable.gikussj+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (3,1,5,"+ R.drawable.gokussd+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (4,1,1,"+ R.drawable.gokussjdos+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (5,1,1,"+ R.drawable.gokussjtres+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (6,1,6,"+ R.drawable.vegetamajin+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (7,1,1,"+ R.drawable.vegetaneutral+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (8,1,1,"+ R.drawable.vegetassj+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (9,1,1,"+ R.drawable.vegetassjdos+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (10,1,5,"+ R.drawable.vegetassjgod+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (11,1,6,"+ R.drawable.majinbuuevil+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (12,1,6,"+ R.drawable.majinbuugood+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (13,1,4,"+ R.drawable.trunksbasefutur+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (14,1,4,"+ R.drawable.trunksbasefutur+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (15,1,4,"+ R.drawable.trunksssj+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (16,1,4,"+ R.drawable.trunksssjdosfutur+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (17,2,4,"+ R.drawable.panpetita+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (18,2,4,"+ R.drawable.pangran+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (19,1,2,"+ R.drawable.firezaimperfect+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (20,1,2,"+ R.drawable.friezaperfect+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (21,1,5,"+ R.drawable.corpetitbase+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (22,2,3,"+ R.drawable.cdieziochobuena+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (23,2,3,"+ R.drawable.cdieziochofuturo+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (24,3,3,"+ R.drawable.cellimperfect+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (25,3,3,"+ R.drawable.cellimperfectodos+")");
        baseDades.execSQL("INSERT INTO IMATGE VALUES (26,3,3," + R.drawable.cellperfect + ")");

        //PERSONATGES
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (1 ,'Goku Super Sayajin', 1,1,2,1,"+R.drawable.gikussj+",30, 35, 34, 20, 40, 45,'When Goku fought with Frieza and Frieza killed Kirilin Goku explodes with all his rage and become the first Super Sayajin in the whole universe',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (2 ,'Goku Super Sayajin 2', 1,1,3,1,"+R.drawable.gokussjdos+",45, 60, 70, 45, 60, 70,'The second transformation of SuperSayajin',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (3 ,'Goku Super Sayajin 3', 1,1,4,1,"+R.drawable.gokussjtres+",22, 44, 10, 50, 12, 32,'The most Strong Transformation if the SuperSayajins form !!',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (4 ,'Goku Super Sayajin God',1,1,5,1,"+R.drawable.gokussd+",45, 60, 70, 30, 60, 70,'The God Transformation for fighting Bills the God of Destruction',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (5 ,'Vegeta Super Sayajin', 1,1,2,3,"+R.drawable.vegetassj+",34, 44, 10, 50, 12, 32,'The first Transformation in SuperSayajin',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (6 ,'Vegeta Super Sayajin God', 1,1,5,1,"+R.drawable.vegetassjgod+",45, 60, 70, 30, 60, 70,'The ultimate transformation of the sayajin mode, The Super Sayajin God Blue!!!',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (7 ,'Majin Vegeta', 1,1,3,2,"+R.drawable.vegetamajin+",45, 60, 70, 30, 60, 70,'When Babidi tells to Vegeta that he would become stronger with the majin magic he accepts, and transforms to a majin warrior moved by evil !',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (8 ,'Majin Buu Evil', 6,1,7,2,"+R.drawable.majinbuuevil+",34, 44, 10, 50, 12, 32,'The ultimate form of pure evil BUU!!',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (9 ,'Majin Buu Lawful',6,1,6,1,"+R.drawable.majinbuugood+",45, 60, 70, 30, 60, 70,'The lawful form of BUU, lives with Herecule',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (10,'Trunks Child Super Sayajin', 4,1,2,1,"+R.drawable.trunksssj+",45, 60, 70, 30, 60, 70,'The Trunks Child Super Sayajin, the strongest child !',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (11,'Trunks Future Super Sayajin 2', 4,1,3,1,"+R.drawable.trunksssjdosfutur+",45, 60, 70, 30, 60, 70,'The Trunks Super Sayajin2 from the future trained in the Korins tower with his father Vegeta',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (12,'Pan Child', 4,2,1,1,"+R.drawable.panpetita+",45, 60, 70, 30, 60, 70,'The Trunks Super Sayajin2 from the future trained in the Korins tower with his father Vegeta',NULL,NULL )");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (13,'Pan GT', 4,2,1,1,"+R.drawable.pangran+",40, 50, 20, 30, 55, 55,'The Trunks Super Sayajin2 from the future trained in the Korins tower with his father Vegeta ',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (14,'Frizer Imperfect Form', 2,1,7,2,"+R.drawable.firezaimperfect+",45, 60, 70, 30, 60, 70,'The first form of a true space warrior!',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (15,'Frizer Perfect Form',2,1,6,2,"+R.drawable.friezaperfect+",45, 60, 70, 30, 60, 70,'The ultimate form of the galactic monarch Frieza',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (16,'Piccolo',5,1,1,3,"+R.drawable.corpetitbase+",45, 60, 70, 30, 60, 70,'The evil that becomes a lawful character and the teacher of martial arts of Gohan',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (17,'C-18',3,2,1,3,"+R.drawable.cdieziochobuena+",45, 60, 70, 30, 60, 70,'The good C-18, married with Kirillin',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (18,'C-18 Future',3,2,1,2,"+R.drawable.cdieziochofuturo+",45, 60, 70, 30, 60, 70,'One of the Dr Gero Androids that causes terror in the future',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (19,'CELL Imperfect Form',3,3,7,2,"+R.drawable.cellimperfect+",45, 60, 70, 30, 60, 70,'The first form of cell, a weak character...',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (20,'CELL Second Form',3,3,7,2,"+R.drawable.cellimperfectodos+",45, 60, 70, 30, 60, 70,'Stronger than the first form of Cell',NULL,NULL)");
        baseDades.execSQL("INSERT INTO PERSONATGE VALUES (21,'CELL Perfect Form',3,3,6,2," + R.drawable.cellperfect + ",34, 44, 10, 50, 12, 32,'The terror personificated, the perfect Form of Cell',NULL,NULL)");
    }

    private void createTables(SQLiteDatabase baseDades , String createSexe, String createOfici, String createRasa, String createAlignment, String createImatge, String createPersonatge) {
        baseDades.execSQL(createSexe);
        baseDades.execSQL(createOfici);
        baseDades.execSQL(createRasa);
        baseDades.execSQL(createAlignment);
        baseDades.execSQL(createImatge);
        baseDades.execSQL(createPersonatge);
    }
/*
    public static List<Personatge> getPersonatges(Context c) {

        int id;
        String mNom;
        int mRasa;
        int mSexe;
        int mOfici;
        int mAlignment;
        int mImatge;
        int mConstitution, mStrength, mDexterity, mWisdom, mIntelligence, mCharisma;
        String mHistoria;
        Integer id_botiga;
        String arxiu_img_desc;
        List<Personatge> pers = new ArrayList<>();


        BaseDadesHelper dbh = new BaseDadesHelper(c, "BaseDades",null, 1);
        SQLiteDatabase baseDades = dbh.getReadableDatabase();
        Cursor cursor = baseDades.rawQuery("select * from personatge;", null);

        while (cursor.moveToNext()) {
            id= cursor.getInt(cursor.getColumnIndex("ID"));
            mNom = cursor.getString(cursor.getColumnIndex("NOM"));
            mRasa = cursor.getInt(cursor.getColumnIndex("RASA_ID"));
            mSexe = cursor.getInt(cursor.getColumnIndex("SEXE_ID"));
            mOfici = cursor.getInt(cursor.getColumnIndex("OFICI_ID"));
            mAlignment = cursor.getInt(cursor.getColumnIndex("ALIGNMENT_ID"));
            mImatge = cursor.getInt(cursor.getColumnIndex("IMATGE"));
            mConstitution = cursor.getInt(cursor.getColumnIndex("CONSTITUTION"));
            mStrength = cursor.getInt(cursor.getColumnIndex("STRENGTH"));
            mDexterity = cursor.getInt(cursor.getColumnIndex("DEXTERITY"));
            mWisdom = cursor.getInt(cursor.getColumnIndex("WISEDOM"));
            mIntelligence = cursor.getInt(cursor.getColumnIndex("INTELLIGENCE"));
            mCharisma = cursor.getInt(cursor.getColumnIndex("CHARISMA"));
            mHistoria = cursor.getString(cursor.getColumnIndex("HISTORY"));
            id_botiga = cursor.getInt(cursor.getColumnIndex("ID_BOTIGA"));
            arxiu_img_desc = cursor.getString(cursor.getColumnIndex("ARXIU_IMG_DESC"));

            pers.add(new Personatge(id, mNom, mRasa, mSexe, mOfici, mAlignment, mImatge, mConstitution, mStrength, mDexterity, mWisdom, mIntelligence, mCharisma, mHistoria, id_botiga, arxiu_img_desc));

        }
        cursor.close();
        return pers;
    }
*/
    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {

/*
        //Se elimina la versión anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS SEXE");
        db.execSQL("DROP TABLE IF EXISTS OFICI");
        db.execSQL("DROP TABLE IF EXISTS RASA");
        db.execSQL("DROP TABLE IF EXISTS ALIGNMENT");
        db.execSQL("DROP TABLE IF EXISTS IMATGE");
        db.execSQL("DROP TABLE IF EXISTS PERSONATGE");

        //Se crea la nueva versión de la tabla
        createTables(db, CreateSexe, CreateOfici, CreateRasa, CreateAlignment, CreateImatge, CreatePersonatge);*/
    }

    public static void esborrarPersonatge(int id, Context c) {
        String where = "id = "+id;
        BaseDadesHelper dbh = new BaseDadesHelper(c, "BaseDades",null, 1);
        SQLiteDatabase baseDades = dbh.getWritableDatabase();
        baseDades.delete("personatge", "id=" + id,null);
    }

    public static int getIdPers(String nom, Context c) {
        BaseDadesHelper dbh = new BaseDadesHelper(c, "BaseDades",null, 1);
        SQLiteDatabase baseDades = dbh.getReadableDatabase();
        Cursor cursor1 = baseDades.rawQuery("select * from personatge where nom ='" + nom + "';", null);
        int id=0;
        while(cursor1.moveToNext()){
            id = cursor1.getInt(cursor1.getColumnIndex("ID"));
        }
        return  id;
    }

    public static boolean existeixPersonatge(String nom,Context c) {
        String mNom = nom.toUpperCase();
        BaseDadesHelper dbh = new BaseDadesHelper(c, "BaseDades",null, 1);
        SQLiteDatabase baseDades = dbh.getReadableDatabase();
        Cursor cursor1 = baseDades.rawQuery("select nom from personatge where upper(nom) = '" + mNom + "';", null);

        try {
            cursor1.moveToFirst();
            String name = cursor1.getString(cursor1.getColumnIndex("NOM"));
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public static void inserirPersonatge(Personatge pers,Context c){
        BaseDadesHelper dbh = new BaseDadesHelper(c, "BaseDades",null, 1);
        SQLiteDatabase baseDades = dbh.getWritableDatabase();
        ContentValues cv = new ContentValues();
        try {
            cv.put("NOM",pers.getmNom());
            cv.put("RASA_ID", pers.getRasa());
            cv.put("SEXE_ID", pers.getmSexe());
            cv.put("OFICI_ID",pers.getmOfici());
            cv.put("ALIGNMENT_ID",pers.getmAlignment());
            cv.put("IMATGE",pers.getmImage());
            cv.put("CONSTITUTION", pers.getmConstitution());
            cv.put("STRENGTH", pers.getmStrength());
            cv.put("DEXTERITY", pers.getmDexterity());
            cv.put("WISEDOM", pers.getmWisdom());
            cv.put("INTELLIGENCE", pers.getmIntelligence());
            cv.put("CHARISMA", pers.getmCharisma());
            cv.put("HISTORY", pers.getmHistoria());
            cv.put("ID_BOTIGA",pers.getId_botiga());
            cv.put("ARXIU_IMG_DESC",pers.getArxiu_img_desc());
            baseDades.insert("personatge", null, cv);
            cv.clear();
        } catch (Exception e) {
            Log.e("PLACAPLACA Insert Pers:", e.getMessage());
        }
    }
}