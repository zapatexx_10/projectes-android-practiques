package net.iesmila.pac1.AssyncTask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;

import net.iesmila.pac1.Activities.ShopActivityLoading;
import net.iesmila.pac1.R;
import net.iesmila.pac1.model.Personatge;
import net.iesmila.pac1.network.PersonatgeDataLoader;

/**
 * Created by Marcc on 24/05/2016.
 */
public class TendaAsyncTask extends AsyncTask<String,Void,Personatge> {

        private ShopActivityLoading mActivity;
        private Context mContext;
        private int mProgress;
        private ProgressBar pbJson;

        public TendaAsyncTask(Context pContext){
             mContext = pContext;
        }

        @Override
        protected void onPreExecute() {
        super.onPreExecute();
        mActivity = (ShopActivityLoading)mContext; //Agafem el context de la activity
        pbJson = (ProgressBar)mActivity.findViewById(R.id.pgrDownloading);
        //pbJson.setMax(100);
            pbJson.setMax(10);
        mActivity.mostrarProgressBar();
    }

        @Override
        protected void onPostExecute(Personatge personatge) {
        super.onPostExecute(personatge);
        if (personatge == null){
            mActivity.mostrarErrorDescarrega();
        }else{
            mActivity.fiDescarrega(personatge);
        }
    }

        @Override
        protected Personatge doInBackground(String... params) {
        try{
            for(mProgress=1;mProgress<100;mProgress++){
                Thread.sleep(20);
                publishProgress();
            }
            publishProgress();
            PersonatgeDataLoader loader = new PersonatgeDataLoader(mActivity.getBaseContext());
            params[0] = "http://vps97640.ovh.net/~m2.mzapata/json_personatges.txt";
            return loader.loadData(params[0]).get(0);
        }catch (Exception e){
            Log.e("OnPostExecute", e.getMessage());
        }
        return null;
    }
    }