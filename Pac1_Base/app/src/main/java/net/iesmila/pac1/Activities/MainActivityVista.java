package net.iesmila.pac1.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.iesmila.pac1.R;
import net.iesmila.pac1.model.Personatge;

/**
 * Created by Marcc on 26/02/2016.
 */
public class MainActivityVista extends Activity {
    private ImageView mImatgePersoantge;
    private TextView mNom;
    private TextView mRasa;
    private TextView mFase;
    private TextView mSexe;
    private TextView mHistoria;
    private ProgressBar mPbChar,mPbStr,mPbDext,mPbConst,mPbInt,mPbWisdom;
    private Button btnAlignment;
    private LinearLayout linearFals;
    private Intent intentPersonatges;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personatge);

        mImatgePersoantge =(ImageView)findViewById(R.id.imvPersonatgeVista);
        mNom= (TextView)findViewById(R.id.txvPersonatge);
        mRasa= (TextView)findViewById(R.id.txvRasa);
        mSexe= (TextView)findViewById(R.id.txvSexe);
        mHistoria= (TextView)findViewById(R.id.txvHistoria);
        btnAlignment=(Button)findViewById(R.id.btnAlignment);
        mPbChar=(ProgressBar)findViewById(R.id.prbCharisma);
        mPbStr=(ProgressBar)findViewById(R.id.prbStrenght);
        mPbDext=(ProgressBar)findViewById(R.id.prbDexterity);
        mPbConst=(ProgressBar)findViewById(R.id.prbConstitution);
        mPbInt=(ProgressBar)findViewById(R.id.prbntelligence);
        mPbWisdom=(ProgressBar)findViewById(R.id.prbWisedom);
        mFase= (TextView)findViewById(R.id.txvFase);
        linearFals=(LinearLayout)findViewById(R.id.linearFals);

        //Rebem el personatge del altre Intent
        intentPersonatges = getIntent();
        Personatge personatge = (Personatge) intentPersonatges.getSerializableExtra("Personatge");

        mNom.setText(personatge.getmNom()); ;
        switch (personatge.getmSexe()){
            case 1: mSexe.setText("Male"); break;
            case 2: mSexe.setText("Female");break;
            case 3: mSexe.setText("Other");
        }
        switch (personatge.getRasa()){
            case 1: mRasa.setText("Sayajin"); break;
            case 2: mRasa.setText("Frieza Race");break;
            case 3: mRasa.setText("Android");break;
            case 4: mRasa.setText("Human Sayajin"); break;
            case 5: mRasa.setText("Namekian");break;
            case 6: mRasa.setText("Majin");break;
        }

        switch (personatge.getmOfici()){
            case 1: mFase.setText("Base"); break;
            case 2: mFase.setText("SSJ1");break;
            case 3: mFase.setText("SSJ2");break;
            case 4: mFase.setText("SSJ3"); break;
            case 5: mFase.setText("SSJGOD");break;
            case 6: mFase.setText("Perfect From");break;
            case 7: mFase.setText("Imperfect From");
        }


        mHistoria.setText(personatge.getmHistoria());
        mImatgePersoantge.setImageResource(personatge.getmImage());

        mPbChar=(ProgressBar)findViewById(R.id.prbCharisma);
        mPbStr=(ProgressBar)findViewById(R.id.prbStrenght);
        mPbDext=(ProgressBar)findViewById(R.id.prbDexterity);
        mPbConst=(ProgressBar)findViewById(R.id.prbConstitution);
        mPbInt=(ProgressBar)findViewById(R.id.prbntelligence);
        mPbWisdom=(ProgressBar)findViewById(R.id.prbWisedom);

        linearFals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        switch (personatge.getmAlignment()){
            case 1: btnAlignment.setBackgroundResource(R.drawable.like); break;
            case 2: btnAlignment.setBackgroundResource(R.drawable.dislike);break;
            case 3: btnAlignment.setBackgroundResource(R.drawable.equals);


        }

        customProgressBar(mPbChar, personatge.getmCharisma());
        customProgressBar(mPbStr, personatge.getmStrength());
        customProgressBar(mPbConst, personatge.getmConstitution());
        customProgressBar(mPbInt, personatge.getmIntelligence());
        customProgressBar(mPbDext, personatge.getmDexterity());
        customProgressBar(mPbWisdom, personatge.getmWisdom());

    }



    public void customProgressBar(ProgressBar pb, int pos){
        final float[] roundedCorners = new float[] { 5, 5, 5, 5, 5, 5, 5, 5 };
        ShapeDrawable pgDrawable = new ShapeDrawable(new RoundRectShape(roundedCorners, null,null));
        String colorBarra = "colors";
        if(pos>=0&&pos<25){
            colorBarra = "#ebd21310";
        }else if(pos>=25&&pos<50){
            colorBarra = "#FF946B23";
        }else if(pos>=50&&pos<75){
            colorBarra = "#4b7901";
        }else if(pos>=75&&pos<=100){
            colorBarra = "#06991c";
        }
        pgDrawable.getPaint().setColor(Color.parseColor(colorBarra));
        ClipDrawable progress = new ClipDrawable(pgDrawable, Gravity.LEFT, ClipDrawable.HORIZONTAL);
        pb.setProgressDrawable(progress);
        pb.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.progress_horizontal));
        pb.setProgress(pos);
    }
}
