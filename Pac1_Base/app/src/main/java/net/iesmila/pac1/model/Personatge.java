package net.iesmila.pac1.model;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.iesmila.pac1.Activities.ListActivity;
import net.iesmila.pac1.Activities.ShopActivity;
import net.iesmila.pac1.R;
import net.iesmila.pac1.bd.BaseDadesHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Personatge implements Serializable{
    int id;
    String mNom;
    int mRasa;
    int mSexe;
    int mOfici;
    int mAlignment;
    int mImage;
    int mConstitution, mStrength, mDexterity, mWisdom, mIntelligence, mCharisma;
    String mHistoria;
    Integer id_botiga = null;
    String arxiu_img_desc=null;

    private static List<Personatge> mPersonatges;
    private static List<Personatge> mPersonatgesStore;

    public static List<Personatge> getPersonatges(Context context) {
        if(mPersonatges==null) {
            mPersonatges= new ArrayList<Personatge>();
            BaseDadesHelper usdbh =
                    new BaseDadesHelper(context, "BaseDades", null, 1);
            int id;
            String mNom;
            int mRasa;
            int mSexe;
            int mOfici;
            int mAlignment;
            int mImatge;
            int mConstitution, mStrength, mDexterity, mWisdom, mIntelligence, mCharisma;
            String mHistoria;
            Integer id_botiga;
            String arxiu_img_desc;

            SQLiteDatabase db = usdbh.getReadableDatabase();

            try {
                Cursor c1 = db.rawQuery("select * from Personatge order by ID", null);
                while (c1.moveToNext()) {
                    id= c1.getInt(c1.getColumnIndex("ID"));
                    mNom = c1.getString(c1.getColumnIndex("NOM"));
                    mRasa = c1.getInt(c1.getColumnIndex("RASA_ID"));
                    mSexe = c1.getInt(c1.getColumnIndex("SEXE_ID"));
                    mOfici = c1.getInt(c1.getColumnIndex("OFICI_ID"));
                    mAlignment = c1.getInt(c1.getColumnIndex("ALIGNMENT_ID"));
                    mImatge = c1.getInt(c1.getColumnIndex("IMATGE"));
                    mConstitution = c1.getInt(c1.getColumnIndex("CONSTITUTION"));
                    mStrength = c1.getInt(c1.getColumnIndex("STRENGTH"));
                    mDexterity = c1.getInt(c1.getColumnIndex("DEXTERITY"));
                    mWisdom = c1.getInt(c1.getColumnIndex("WISEDOM"));
                    mIntelligence = c1.getInt(c1.getColumnIndex("INTELLIGENCE"));
                    mCharisma = c1.getInt(c1.getColumnIndex("CHARISMA"));
                    mHistoria = c1.getString(c1.getColumnIndex("HISTORY"));
                    id_botiga = c1.getInt(c1.getColumnIndex("ID_BOTIGA"));
                    arxiu_img_desc = c1.getString(c1.getColumnIndex("ARXIU_IMG_DESC"));
                    mPersonatges.add(new Personatge(id, mNom, mRasa, mSexe, mOfici, mAlignment, mImatge, mConstitution, mStrength, mDexterity, mWisdom, mIntelligence, mCharisma, mHistoria, id_botiga, arxiu_img_desc));

                    Log.d("MARC", c1.getString(c1.getColumnIndex("NOM")));
                }
            }catch(Exception e){
                Log.e("BERNAT", e.getMessage());
            }
            db.close();
        }
        return mPersonatges;
    }

    public Personatge() {}

    public Personatge(int id, String mNom, int mRasa, int mSexe, int mOfici, int mAlignment, int mImage, int mConstitution, int mStrength, int mDexterity, int mWisdom, int mIntelligence, int mCharisma, String mHistoria, Integer id_botiga, String arxiu_img_desc) {
        this.id = id;
        this.mNom = mNom;
        this.mRasa = mRasa;
        this.mSexe = mSexe;
        this.mOfici = mOfici;
        this.mAlignment = mAlignment;
        this.mImage = mImage;
        this.mConstitution = mConstitution;
        this.mStrength = mStrength;
        this.mDexterity = mDexterity;
        this.mWisdom = mWisdom;
        this.mIntelligence = mIntelligence;
        this.mCharisma = mCharisma;
        this.mHistoria = mHistoria;
        this.id_botiga = id_botiga;
        this.arxiu_img_desc = arxiu_img_desc;
    }

    public Personatge(String mNom, int mRasa, int mSexe,int mOfici, int mAlignment, int mImage,int mConstitution, int mStrength,
                      int mDexterity,int mWisdom, int mIntelligence, int mCharisma,String mHistoria, int mId_store,String mRuta_img) {


        this.mNom = mNom;
        this.mRasa = mRasa;
        this.mSexe = mSexe;
        this.mOfici = mOfici;
        this.mAlignment = mAlignment;
        this.mImage = mImage;
        this.mConstitution = mConstitution;
        this.mStrength = mStrength;
        this.mDexterity = mDexterity;
        this.mWisdom = mWisdom;
        this.mIntelligence = mIntelligence;
        this.mCharisma = mCharisma;
        this.mHistoria = mHistoria;
        this.id_botiga = mId_store;
        this.arxiu_img_desc = mRuta_img;
    }

    public static void setmPersonatges(ArrayList<Personatge> mPersonatges) {
        Personatge.mPersonatges = mPersonatges;
    }

    public static List<Personatge> getPersonatgesActualitzats(Context c){
        mPersonatges = null;
       return getPersonatges(c);

    }

    public static void setmPersonatgesStore(List<Personatge> pPersonatgesStore) {
        mPersonatgesStore = pPersonatgesStore;
    }

    public static List<Personatge> getPersonatgesStore(Context c) {
        return mPersonatgesStore;
    }


    public String getmNom() {
        return mNom;
    }

    public void setmNom(String mNom,Context context, int idPersonatge) {
        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET NOM='" + mNom +"' WHERE id=" + idPersonatge+"";
        db.execSQL(sql);
        db.close();
        this.mNom = mNom;
    }

    public void setmHistoria(String mHistoria,Context context, int idPersonatge) {
        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET HISTORY='" + mHistoria+"' WHERE id=" + idPersonatge+"";
        db.execSQL(sql);
        db.close();
        this.mHistoria = mHistoria;
    }

    public String getNomOfici(Context context,int id){
        BaseDadesHelper usdbh =  new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getReadableDatabase();
        String mNom =null;
        try {
            Cursor c1 = db.rawQuery("select o.name as NomOficiPersona from Personatge p, ofici o where p.ofici_id = o.id and p.id="+id+";", null);
            while (c1.moveToNext()) {
                mNom = c1.getString(c1.getColumnIndex("NomOficiPersona"));
            }
        }catch(Exception e){
            Log.e("BERNAT", e.getMessage());
        }
        db.close();
        return mNom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRasa() {
        return mRasa;
    }

    public void setmRasa(int mRasa,Context context,int idPersonatge) {
        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET RASA_ID=" + mRasa + " WHERE id=" + idPersonatge + "";
        db.execSQL(sql);
        db.close();
        this.mRasa = mRasa;
    }

    public int getmSexe() {
        return mSexe;
    }

    public void setmSexe(int mSexe,Context context, int idPersonatge) {
        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET SEXE_ID=" + mSexe + " WHERE id=" + idPersonatge + "";
        db.execSQL(sql);
        db.close();
        this.mSexe = mSexe;
    }

    public int getmOfici() {
        return mOfici;
    }

    public void setmOfici(int mOfici,Context context, int idPersonatge) {

        this.mOfici = mOfici;BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET OFICI_ID=" + mOfici + " WHERE id=" + idPersonatge + "";
        db.execSQL(sql);
        db.close();
    }

    public int getmAlignment() {
        return mAlignment;
    }

    public void setmAlignment(int mAlignment,Context context,int idPersonatge) {

        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET ALIGNMENT_ID=" + mAlignment +" WHERE id=" + idPersonatge+"";
        db.execSQL(sql);
        db.close();
        this.mAlignment = mAlignment;
    }

    public int getmImage() {
        return mImage;
    }

    public void setmImage(int mImage,Context context,int idPersonatge) {
        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET IMATGE=" + mImage +" WHERE id=" + idPersonatge+"";
        db.execSQL(sql);
        db.close();
        this.mImage = mImage;
    }

    public int getmConstitution() {
        return mConstitution;
    }

    public void setmConstitution(int mConstitution,Context context, int idPersonatge) {
        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET CONSTITUTION=" +mConstitution +" WHERE id=" + idPersonatge+"";
        db.execSQL(sql);
        db.close();
        this.mConstitution = mConstitution;
    }

    public int getmStrength() {
        return mStrength;
    }

    public void setmStrength(int mStrength,Context context, int idPersonatge) {
        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET STRENGTH=" +mStrength +" WHERE id=" + idPersonatge+"";
        db.execSQL(sql);
        db.close();
        this.mStrength = mStrength;
    }

    public int getmDexterity() {
        return mDexterity;
    }

    public void setmDexterity(int mDexterity,Context context, int idPersonatge) {

        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET DEXTERITY=" + mDexterity +" WHERE id=" + idPersonatge+"";
        db.execSQL(sql);
        db.close();
        this.mDexterity = mDexterity;
    }

    public int getmWisdom() {
        return mWisdom;
    }

    public void setmWisdom(int mWisdom,Context context, int idPersonatge) {
            BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
            SQLiteDatabase db = usdbh.getWritableDatabase();
            String sql = "UPDATE Personatge SET WISEDOM=" + mWisdom +" WHERE id=" + idPersonatge+"";
            db.execSQL(sql);
            db.close();
        this.mWisdom = mWisdom;
    }

    public int getmIntelligence() {
        return mIntelligence;
    }

    public void setmIntelligence(int mIntelligence,Context context, int idPersonatge) {
        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        Cursor c1 = db.rawQuery("select * from Personatge where id= "+idPersonatge+"", null);
        String sql = "UPDATE Personatge SET INTELLIGENCE=" + mIntelligence +" WHERE id=" + idPersonatge+"";
        db.execSQL(sql);
        db.close();
        this.mIntelligence = mIntelligence;
    }

    public int getmCharisma() {
        return mCharisma;
    }

    public void setmCharisma(int mCharisma,Context context, int idPersonatge) {
        BaseDadesHelper usdbh = new BaseDadesHelper(context, "BaseDades", null, 1);
        SQLiteDatabase db = usdbh.getWritableDatabase();
        String sql = "UPDATE Personatge SET CHARISMA=" + mCharisma +" WHERE id=" + idPersonatge+"";
        db.execSQL(sql);
        db.close();
        this.mCharisma = mCharisma;
    }

    public String getmHistoria() {
        return mHistoria;
    }

    public Integer getId_botiga() {
        return id_botiga;
    }

    public void setId_botiga(Integer id_botiga) {
        this.id_botiga = id_botiga;
    }

    public String getArxiu_img_desc() {
        return arxiu_img_desc;
    }

    public void setArxiu_img_desc(String arxiu_img_desc) {
        this.arxiu_img_desc = arxiu_img_desc;
    }

    @Override
    public String toString() {
        return "Personatge{" +
                "id=" + id +
                ", mNom='" + mNom + '\'' +
                ", mRasa=" + mRasa +
                ", mSexe=" + mSexe +
                ", mOfici=" + mOfici +
                ", mAlignment=" + mAlignment +
                ", mImage=" + mImage +
                ", mConstitution=" + mConstitution +
                ", mStrength=" + mStrength +
                ", mDexterity=" + mDexterity +
                ", mWisdom=" + mWisdom +
                ", mIntelligence=" + mIntelligence +
                ", mCharisma=" + mCharisma +
                ", mHistoria='" + mHistoria + '\'' +
                ", id_botiga=" + id_botiga +
                ", arxiu_img_desc=" + arxiu_img_desc +
                '}';
    }

    public boolean comprat() {
        return this.getmImage()==-1;
}


}
