package net.iesmila.pac1.model;


import java.io.Serializable;
import java.util.ArrayList;

public class Ofici implements Serializable {

    private int mCodi;
    private String mOfici;
    private static ArrayList<Ofici> mOficis;

    public static ArrayList<Ofici> getOficis(){

        if(mOficis==null) {
            mOficis= new ArrayList<Ofici>();
        }
        return mOficis;
    }

    public static Ofici getOficiPerCodi(int codi)
    {
        ArrayList<Ofici> oficis = getOficis();
        for(Ofici f :oficis) {
            if(f.getCodi()==codi) return f;
        }
        throw new IndexOutOfBoundsException();
    }

    public Ofici(int mCodi, String mOfici) {
        this.mCodi = mCodi;
        this.mOfici = mOfici;
    }

    public int getCodi() {
        return mCodi;
    }

    public String getOfici() {
        return mOfici;
    }

    public void setOfici(String mOfici) {
        this.mOfici = mOfici;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ofici ofici = (Ofici) o;

        return mCodi == ofici.mCodi;

    }

    @Override
    public int hashCode() {
        return mCodi;
    }

    @Override
    public String toString() {

        return mOfici;
    }
}