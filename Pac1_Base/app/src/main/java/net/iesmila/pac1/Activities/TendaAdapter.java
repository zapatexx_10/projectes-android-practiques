package net.iesmila.pac1.Activities;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.iesmila.pac1.LazyLoading.ImageLoader;
import net.iesmila.pac1.R;
import net.iesmila.pac1.bd.BaseDadesHelper;
import net.iesmila.pac1.model.Personatge;

import java.util.List;

/**
 * Created by Marcc on 28/04/2016.
 */
public class TendaAdapter extends ArrayAdapter<Personatge> {

    private static int index;
    private static ViewGroup parentListView = null;
    private ImageLoader imLoader;
    private static LinearLayout layoutPersStore;
    private List<Personatge> mLlistaPersonatges;

    public TendaAdapter(Context context, List<Personatge> pLlistaPers){
        super(context, R.layout.fila_carta_store, R.id.tvNomListViewStore, pLlistaPers);
        mLlistaPersonatges = pLlistaPers;
        imLoader = new ImageLoader(context);
    }

    private View.OnClickListener mOnButtonClicked = new View.OnClickListener() {

        public void onClick(View v) {

            layoutPersStore  =(LinearLayout) v;

            Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.animacio_item_fila);
            v.setAnimation(anim);
            anim.start();

            index = (int)layoutPersStore.getTag();
        }
    };

    @Override
    public View getView(int position, View vista, ViewGroup parent) {
        parentListView = parent;
        final LinearLayout layoutPersStore;

        if (vista !=null){
            layoutPersStore = (LinearLayout)vista;
        }else{
            LayoutInflater factory = LayoutInflater.from(this.getContext());
            layoutPersStore = (LinearLayout)factory.inflate(R.layout.fila_carta_store,parent,false);
        }
        index = position;

        ImageView imgvFoto = (ImageView)layoutPersStore.findViewById(R.id.ivfilaStoreImatge);
        ImageView imgvRasa = (ImageView)layoutPersStore.findViewById(R.id.ivfilaStoreRasa);
        ImageView imgAligment = (ImageView)layoutPersStore.findViewById(R.id.ivAligmentListView);

        TextView tvNom = (TextView)layoutPersStore.findViewById(R.id.tvNomListViewStore);
        TextView tvOfici = (TextView)layoutPersStore.findViewById(R.id.tvStoreOfici);

        Personatge aux = mLlistaPersonatges.get(index);

        String foto = aux.getArxiu_img_desc();

        String url= "http://vps97640.ovh.net/~m2.mzapata/";
        Log.d("ICON RUTA:\t", url + foto + " +++++++++++++++++++++++++++++++++++++");
        imLoader.DisplayImage(url+foto, imgvFoto);

        /*
        *
        * Fer les comprovacions que feia al Personatge Adapter depenent de quin
        * tipus de Rasa,sexe, aligment agafar una foto o una altre....
        *
        * */

        switch (aux.getmOfici()){
            case 1:tvOfici.setText("Base"); break;
            case 2:tvOfici.setText("SSJ1"); break;
            case 3:tvOfici.setText("SSJ2"); break;
            case 4:tvOfici.setText("SSJ3"); break;
            case 5:tvOfici.setText("SSJGOD"); break;
            case 6:tvOfici.setText("Perfect form");break;
            case 7:tvOfici.setText("Imperfect form");
        }


        switch (aux.getmAlignment()){
            case 1:imgAligment.setImageResource(R.drawable.like); break;
            case 2:imgAligment.setImageResource(R.drawable.dislike);break;
            case 3:imgAligment.setImageResource(R.drawable.equals);
        }


        switch (aux.getRasa()){
            case 1: imgvRasa.setImageResource(R.drawable.sayajinlogo); break;
            case 2: imgvRasa.setImageResource(R.drawable.friezaforcelogo);break;
            case 3: imgvRasa.setImageResource(R.drawable.redribbonlogo);break;
            case 4: imgvRasa.setImageResource(R.drawable.humanslogo);break;
            case 6: imgvRasa.setImageResource(R.drawable.majinlogo);break;
            case 5: imgvRasa.setImageResource(R.drawable.namekianlogo);
        }
        tvNom.setText(aux.getmNom());

        //Final
        layoutPersStore.setTag(index);
        layoutPersStore.setOnClickListener(mOnButtonClicked);

        return layoutPersStore;

    }

    public static int getmPos(){
        return index;
    }

    public void esborrarPersonatgeStore(int index) {
        BaseDadesHelper.inserirPersonatge(mLlistaPersonatges.get(index),getContext());
        mLlistaPersonatges.remove(index);
        Personatge.setmPersonatgesStore(mLlistaPersonatges);
        this.notifyDataSetChanged();
    }


}
