package net.iesmila.pac1.Activities;

import android.app.*;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import net.iesmila.pac1.R;

/**
 * Created by Marcc on 21/03/2016.
 */
public class ActivityChooseOption extends Activity {
    private ImageView mImatgeShop;
    private ImageView mImatgePersoantges;
    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        i = getIntent();
        setContentView(R.layout.choose_mode);
         MediaPlayer mediaPlayer;
        mediaPlayer = MediaPlayer.create(this, R.raw.boladedracz);
        mediaPlayer.setLooping(true);
        mediaPlayer.setVolume(100, 100);
        mediaPlayer.start();

        mImatgeShop= (ImageView) findViewById(R.id.imvShop);
        mImatgePersoantges= (ImageView) findViewById(R.id.imvCharacters);

        mImatgePersoantges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityChooseOption.this, ListActivity.class);
                startActivity(i);
            }
        });

        mImatgeShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityChooseOption.this, ShopActivityLoading.class);
                startActivity(i);
            }
        });
    }

}